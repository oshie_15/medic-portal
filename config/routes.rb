# frozen_string_literal: true

Rails.application.routes.draw do
  get '/member_details' => 'members#index'

  # config/routes.rb
  root 'welcomes#index'


  namespace :api do
    namespace :v1 do
      resources :patients
    end
  end

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
end
