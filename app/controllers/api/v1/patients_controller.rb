# frozen_string_literal: true

module Api
  module V1
    class PatientsController < ApiController
      load_and_authorize_resource
      before_action :set_patient, only: %i[show update destroy]
      def index
        @patients = Patient.all

        # we use following code when every user want to see their own data @patients = Patient.accessible_by(current_ability)

        # @patients = current_user.patients
        @patients_by_day = Patient.group_by_day(:created_at).count
        render json: { patients: @patients, patients_by_day: @patients_by_day }, status: :ok
      end

      def show
        render json: @patient, status: :ok
      end

      def create
        @patient = Patient.new(patient_params)

        # @patient = current_user.patients.new(patient_params)
        if @patient.save
          render json: @patient, status: :ok
        else
          render json: { data: @patient.errors.full_messages, status: 'failed' },
                 status: :unprocessable_entity
        end
      end

      def update
        if @patient.update(patient_params)
          render json: @patient, status: :ok
        else
          render json: { data: @patient.errors.full_messages, status: 'failed' },
                 status: :unprocessable_entity
        end
      end

      def destroy
        if @patient.destroy
          render json: { data: 'Patients information deleted Successfully', status: 'success' },
                 status: :ok
        else
          render json: { data: 'Something went wrong', status: 'failed' }
        end
      end

      private

      def set_patient
        @patient = Patient.find(params[:id])
      # @patient = current_user.patients.find(params[:id])
      rescue ActiveRecord::RecordNotFound => e
        render json: e.message, status: :unauthorized
      end

      def patient_params
        params.require(:patient).permit(:name, :age, :user_id)
      end
    end
  end
end
