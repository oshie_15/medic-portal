# frozen_string_literal: true

class Patient < ApplicationRecord
  belongs_to :user
  validates :name, presence: true
  validates :age, presence: true
end
