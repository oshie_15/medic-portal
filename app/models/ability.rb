# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, :all
    elsif user.receptionist?
      can %i[read update create destroy], Patient, user_id: user.id
    elsif user.doctor?
      can [:read], Patient # Doctor can read all Patient records
    end
  end
end
